angular.module('projecthub', [
  'ngCookies', 'ngAnimate', 'ngSanitize', 'ui.router', 'ui.bootstrap.showErrors', 'textAngular',
  'projecthub.controllers', 'projecthub.directives', 'projecthub.services'])

  .run(function ($http, $rootScope, $location, $cookies, $state, $stateParams) {

    $http.defaults.headers.common['Cache-Control'] = 'no-cache';
    $http.defaults.headers.common['X-CSRFToken'] = $cookies.csrftoken;

    // see http://angular-ui.github.io/ui-router/sample/app/app.js
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
  })

  .config(function ($httpProvider, $stateProvider, $urlRouterProvider, $locationProvider, $provide, uiSelectConfig) {

    $httpProvider.defaults.headers.patch = {
      'Content-Type': 'application/json;charset=utf-8'
    };

    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
      $httpProvider.defaults.headers.get = {};
    }
    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = '0';


    // see https://github.com/angular-ui/ui-router/issues/582
    // Bug in $state.reload() - resolves are refreshed, but Controller is not re-initialised
    $provide.decorator('$state', function ($delegate) {
      $delegate.reinit = function () {
        this.go('.', null, { reload: true });
      };
      return $delegate;
    });

    // set theme for dropdowns
    uiSelectConfig.theme = 'bootstrap';

    // set default toolbar for rich text editor
    $provide.decorator('taOptions', ['$delegate', function (taOptions) {
      taOptions.toolbar = [
        ['bold', 'italics', 'underline'],['ul', 'ol']
      ];
      taOptions.classes = {
                focussed: 'focussed',
                toolbar: 'btn-toolbar',
                toolbarGroup: 'btn-group',
                toolbarButton: 'btn btn-default btn-sm',
                toolbarButtonActive: 'active',
                disabled: 'disabled',
                textEditor: 'form-control',
                htmlEditor: 'form-control'
            };
      return taOptions;
    }]);

    // use the HTML5 History API
    $locationProvider.html5Mode(true);

    $urlRouterProvider.when("", "/hub/activity");
    $urlRouterProvider.when("/", "/hub/activity");
    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/hub/activity');

    $stateProvider
      .state('hub', {
        abstract: true,
        url: "/hub",
        templateUrl: 'templates/hub.html',
        controller: 'HubCtrl'
      })

      .state('hub.activity', {
        url: "/activity",
        templateUrl: 'templates/activity.html',
        controller: 'ActivityCtrl',
        resolve: {
          viewDetails: function () {
            return {
              title: 'Recent activity'
            }
          }
        }
      })

      .state('hub.project', {
        url: "/:projectId",
        templateUrl: 'templates/project/detail.html',
        controller: 'ViewProjectCtrl',
        resolve: {
          project: function ($stateParams, ProjectService) {
            return ProjectService.one($stateParams.projectId).get();
          },
          discussions: function($stateParams, DiscussionService) {
            return DiscussionService.getList();
          },
          viewDetails: function (project) {
            return {
              title: project.title
            }
          }
        }
      })

      .state('hub.discussion', {
        url: "/:projectId/discussion/:discussionId?new",
        templateUrl: 'templates/discussion/detail.html',
        controller: 'ViewDiscussionCtrl',
        resolve: {
          discussion: function ($stateParams, DiscussionService) {
            return DiscussionService.one($stateParams.discussionId).get();
          },
          viewDetails: function (discussion) {
            return {
              title: discussion.topic
            }
          }
        }
      })

      .state('hub.addDiscussion', {
        url: "/:projectId/discussion",
        templateUrl: 'templates/discussion/form.html',
        controller: 'AddDiscussionCtrl',
        resolve: {
          viewDetails: function () {
            return {
              title: 'New discussion'
            }
          }
        }
      })
    ;
  })

/**
 * AngularJS default filter with the following expression:
 * "person in people | filter: {name: $select.search, age: $select.search}"
 * performs a AND between 'name: $select.search' and 'age: $select.search'.
 * We want to perform a OR.
 */
  .filter('propsFilter', function () {
    return function (items, props) {
      var out = [];

      if (angular.isArray(items)) {
        items.forEach(function (item) {
          var itemMatches = false;

          var keys = Object.keys(props);
          for (var i = 0; i < keys.length; i++) {
            var prop = keys[i];
            var text = props[prop].toLowerCase();
            if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
              itemMatches = true;
              break;
            }
          }

          if (itemMatches) {
            out.push(item);
          }
        });
      } else {
        // Let the output be the input untouched
        out = items;
      }

      return out;
    };
  });
angular.module('projecthub.controllers', ['ui.bootstrap', 'ui.select', 'textAngular', 'projecthub.services'])

  .controller('NavCtrl', function ($scope, $timeout, EnvService, ProjectService, CurrentUserService) {

    $scope.nav = {
      'isHidden': false
    };

    $scope.userNav = {
      'isOpen': false
    };

    EnvService.get().then(function (env) {
      $scope.env = env;
    });

    CurrentUserService.get().then(function (user) {
      $scope.user = user;
    });

    ProjectService.getList().then(function (projects) {
      $scope.projects = projects;
    });

    $scope.toggleNav = function ($event) {
      $event.preventDefault();
      $event.stopPropagation();
      $scope.nav.isHidden = !$scope.nav.isHidden;
    };

    $scope.toggleUserNav = function ($event) {
      $event.preventDefault();
      $event.stopPropagation();
      $scope.userNav.isOpen = !$scope.userNav.isOpen;
    };

    $scope.activateUserNavToggle = function ($event) {
      $event.preventDefault();
      $event.stopPropagation();

      // for ui-bootstrap dropdowns to work, we need to activate via the anchor click event
      var anchor = document.getElementById('user-nav-toggle');

      // see http://stackoverflow.com/questions/22447374/how-to-trigger-ng-click-angularjs-programmatically
      $timeout(function () {
        angular.element(anchor).triggerHandler('click');
      }, 0);
    };
  })

  .controller('HubCtrl', function ($scope, $rootScope, $state) {

  })

  .controller('ActivityCtrl', function ($scope, $rootScope, $state, viewDetails) {
    $rootScope.viewDetails = viewDetails;

    // TODO: List latest discussions
  })

  .controller('ViewProjectCtrl', function ($scope, $rootScope, $stateParams, viewDetails, project, discussions) {
    $rootScope.viewDetails = viewDetails;
    $scope.project = project;
    $scope.project.discussions = discussions;
  })

  .controller('ViewDiscussionCtrl', function ($scope, $rootScope, $state, $stateParams, viewDetails, discussion, RESTService) {

    $rootScope.viewDetails = viewDetails;

    // TODO: Refactor - need isNewDiscussion and isNewComment
    $scope.isNew = $stateParams.new;

    $scope.newComment = {};

    $scope.discussion = discussion;

    $scope.addComment = function () {
      $scope.$broadcast('show-errors-check-validity');
      if ($scope.discussionCommentForm.$valid) {

        // TODO: can we externalise this?
        var discussionCommentsService =
          RESTService.service('comments', RESTService.one('discussions', $stateParams.discussionId));

        discussionCommentsService.post($scope.newComment).then(function () {
          $state.reinit();
        });
      }
    }
  })

  .controller('AddDiscussionCtrl', function (
    $scope, $rootScope, $state, $stateParams, viewDetails, DiscussionService, ProjectService) {

    $rootScope.viewDetails = viewDetails;

    $scope.projectMembers = [];

    // get list of project members, removing thew logged in user. This gives us the list of potential discussion members
    ProjectService.one($stateParams.projectId).get().then(function (project) {
      $scope.projectMembers = project.participants;
      _.remove($scope.projectMembers, function (member) {
        return member.id === $scope.user.id;
      });
    });

    initialise();

    function initialise() {
      $scope.discussion = {
        project: $stateParams.projectId,
        members: [],
        comment: null
      };
    }

    $scope.save = function () {
      $scope.$broadcast('show-errors-check-validity');
      if ($scope.discussionForm.$valid) {
        // NOTE: Discussion objects don't actually have a comment attribute, but the first message is submitted with the form
        // at creation time. The back-end takes care of saving the nested model Discussion->Message
        DiscussionService.post($scope.discussion).then(function (discussion) {
          // reset model/form for next use
          initialise();
          // indicate that this is a new discussion so that view can animate
          $state.go('hub.discussion',
            {projectId: $stateParams.projectId, discussionId: discussion.id, new: true});
        });
      }
    }
  })
;
angular.module('projecthub.directives', [])

  .directive('autoFocus', function () {
    return {
      restrict: 'A', // only matches attribute name

      link: function (scope, element) {
        element[0].focus();
      }
    }
  })
;
angular.module('projecthub.services', ['restangular'])

  .factory('RESTService', function (Restangular) {
    return Restangular.withConfig(function (RestangularConfigurer) {
      RestangularConfigurer.setBaseUrl('/api/v1/');
      // Django expects trailing slashes
      RestangularConfigurer.setRequestSuffix('/');
      // DRF returns data nested with meta data for lists..
      RestangularConfigurer.addResponseInterceptor(function (data) {
        if (data.results != undefined) {
          var extractedData = data.results;
          extractedData.meta = {
            count: data.count,
            next: data.next,
            previous: data.previous
          };
          return extractedData;
        }
        else {
          return data;
        }
      });
    });
  })

  .factory('EnvService', function (RESTService) {
    return  {
      get: function () {
        return RESTService.oneUrl('user', '/api/v1/env/').get().then(function (env) {
          return env;
        })
      }
    }
  })

  .factory('CurrentUserService', function (RESTService) {
    return  {
      get: function () {
        return RESTService.oneUrl('user', '/api/v1/users/current/').get().then(function (user) {
          return user;
        })
      }
    }
  })

  .factory('UserService', function (RESTService) {
    return RESTService.service('users');
  })

  .factory('ProjectService', function (RESTService) {
    return RESTService.service('projects');
  })

  .factory('DiscussionService', function (RESTService) {
    return RESTService.service('discussions');
  })
;