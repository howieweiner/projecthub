angular.module('projecthub').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('templates/activity.html',
    "<!-- main content -->\n" +
    "<div class=\"row main-content\">\n" +
    "    <div class=\"col-lg-12\">\n" +
    "        <h4>activity...</h4>\n" +
    "    </div>\n" +
    "</div>\n" +
    "<!-- #main content -->"
  );


  $templateCache.put('templates/discussion/detail.html',
    "<!-- main content -->\n" +
    "<div class=\"row main-content\">\n" +
    "    <div class=\"col-lg-12\">\n" +
    "        <div ng-class=\"isNew ? 'animated fadeIn' : ''\">\n" +
    "            <p><a ui-sref=\"hub.project( {projectId: discussion.project.id} )\">Back to project</a></p>\n" +
    "            <ul>\n" +
    "                <li ng-repeat=\"comment in discussion.comments\">{{ comment.text }}</li>\n" +
    "            </ul>\n" +
    "            <hr/>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "<!-- #main content -->\n" +
    "\n" +
    "<!-- footer -->\n" +
    "<footer class=\"footer\">\n" +
    "    <div class=\"row\">\n" +
    "        <div class=\"col-lg-12\">\n" +
    "\n" +
    "            <form role=\"form\" novalidate name=\"discussionCommentForm\">\n" +
    "                <div class=\"form-group\" show-errors>\n" +
    "                    <label for=\"comment\">Comment</label>\n" +
    "\n" +
    "                    <div text-angular\n" +
    "                     name=\"comment\"\n" +
    "                     ng-model=\"newComment.text\"\n" +
    "                     placeholder=\"Add a comment..\"\n" +
    "                     required=\"true\"\n" +
    "                     ng-required=\"true\"></div>\n" +
    "\n" +
    "                </div>\n" +
    "                <button type=\"submit\" class=\"btn btn-primary\" ng-click=\"addComment(discussionCommentForm)\">Add Comment\n" +
    "                </button>\n" +
    "            </form>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</footer>\n" +
    "<!-- #footer -->"
  );


  $templateCache.put('templates/discussion/form.html',
    "<!-- main content -->\n" +
    "<div class=\"row main-content\">\n" +
    "    <div class=\"col-lg-6\">\n" +
    "        <h4>Add a new discussion</h4>\n" +
    "\n" +
    "        <form role=\"form\" novalidate name=\"discussionForm\">\n" +
    "\n" +
    "            <div class=\"form-group\" show-errors>\n" +
    "                <label for=\"subject\">Subject</label>\n" +
    "                <input type=\"text\" class=\"form-control\" name=\"subject\" placeholder=\"Subject of discussion\"\n" +
    "                       ng-model=\"discussion.topic\" required auto-focus/>\n" +
    "            </div>\n" +
    "\n" +
    "            <div class=\"form-group\" show-errors>\n" +
    "                <label for=\"members\">Members</label>\n" +
    "\n" +
    "                <ui-select multiple name=\"members\" ng-model=\"discussion.members\" required=\"true\" ng-required=\"true\">\n" +
    "                    <ui-select-match placeholder=\"Add a member...\">{{ $item.full_name }}</ui-select-match>\n" +
    "                    <ui-select-choices\n" +
    "                            repeat=\"user in projectMembers | propsFilter: {first_name: $select.search, last_name: $select.search}\">\n" +
    "                        <div ng-bind-html=\"user.full_name | highlight: $select.search\"></div>\n" +
    "                    </ui-select-choices>\n" +
    "                </ui-select>\n" +
    "            </div>\n" +
    "\n" +
    "            <div class=\"form-group\" show-errors>\n" +
    "                <label for=\"comment\">Comment</label>\n" +
    "\n" +
    "                <div text-angular\n" +
    "                     name=\"comment\"\n" +
    "                     ng-model=\"discussion.comment\"\n" +
    "                     placeholder=\"Add a comment..\"\n" +
    "                     required=\"true\"\n" +
    "                     ng-required=\"true\"></div>\n" +
    "            </div>\n" +
    "\n" +
    "            <button type=\"submit\" class=\"btn btn-primary\" ng-click=\"save()\">Add</button>\n" +
    "            <a ui-sref=\"hub.project({ projectId: discussion.project})\" class=\"btn btn-default\">Cancel</a>\n" +
    "        </form>\n" +
    "    </div>\n" +
    "</div>\n" +
    "<!-- #main content -->\n"
  );


  $templateCache.put('templates/hub.html',
    "<!-- main head -->\n" +
    "<div class=\"row main-header\">\n" +
    "    <div class=\"col-lg-12\">\n" +
    "        <a ng-click=\"toggleNav($event)\" class=\"toggle-link\" ng-show=\"nav.isHidden\">\n" +
    "            <i class=\"fa fa-bars\"></i>\n" +
    "        </a>\n" +
    "        <div class=\"page-title\" ng-bind=\"viewDetails.title\"></div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "<!-- #main head -->\n" +
    "\n" +
    "<section ui-view></section>"
  );


  $templateCache.put('templates/project/detail.html',
    "<!-- main content -->\n" +
    "<div class=\"row main-content\">\n" +
    "    <div class=\"col-lg-12\">\n" +
    "        <div>\n" +
    "            <a class=\"btn btn-primary\" ui-sref=\"hub.addDiscussion({ projectId: project.id })\">New discussion</a>\n" +
    "        </div>\n" +
    "        <hr/>\n" +
    "        <ul class=\"list-group discussion-list\">\n" +
    "            <li class=\"list-group-item\" ng-repeat=\"discussion in project.discussions\">\n" +
    "                <a ui-sref=\"hub.discussion({ projectId: project.id, discussionId: discussion.id })\">\n" +
    "                    {{ discussion.topic }}\n" +
    "                </a>\n" +
    "            </li>\n" +
    "        </ul>\n" +
    "    </div>\n" +
    "</div>\n" +
    "<!-- #main content -->"
  );

}]);
