from django.contrib import admin
from sorl.thumbnail.admin import AdminImageMixin
from hub.models import Client, Project


class ClientAdmin(AdminImageMixin, admin.ModelAdmin):
    pass


class ProjectAdmin(admin.ModelAdmin):
    pass


admin.site.register(Client, ClientAdmin)
admin.site.register(Project, ProjectAdmin)
