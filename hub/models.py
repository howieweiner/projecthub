from django.db import models
from sorl.thumbnail import ImageField
from project_hub import settings


class Client(models.Model):
    name = models.CharField(max_length=100, unique=True)
    logo = ImageField(upload_to='clients/logos', null=True, blank=True)

    def __unicode__(self):
        return self.name


class Project(models.Model):
    title = models.CharField(max_length=100)
    client = models.ForeignKey(Client, related_name='projects')
    active = models.BooleanField(default=True)
    participants = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name='projects')
    archived = models.BooleanField(default=False)

    def __unicode__(self):
        return self.title


class Discussion(models.Model):
    topic = models.CharField(max_length=100)
    project = models.ForeignKey(Project, related_name='discussions')
    created = models.DateTimeField(auto_now_add=True)
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='owned_discussions')
    members = models.ManyToManyField(settings.AUTH_USER_MODEL,
                                     through='DiscussionMember',
                                     related_name='belongs_to_discussions')
    archived = models.BooleanField(default=False)

    class Meta:
        ordering = ('-created',)

    def __unicode__(self):
        return self.topic


class DiscussionMember(models.Model):
    discussion = models.ForeignKey(Discussion)
    member = models.ForeignKey(settings.AUTH_USER_MODEL)
    muted = models.BooleanField(default=False)


class DiscussionComment(models.Model):
    text = models.TextField()
    discussion = models.ForeignKey(Discussion, related_name='comments')
    created = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL)

    class Meta:
        ordering = ('created',)

    def __unicode__(self):
        return self.text


class DiscussionCommentAttachment(models.Model):
    comment = models.ForeignKey(DiscussionComment, related_name='attachments')
    file = models.FileField(upload_to='discussions/comments/%Y/%m/%d')
