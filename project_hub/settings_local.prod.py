ENV = 'prod'
SITE_ADDRESS = 'http://localhost:8000'

DEBUG = False
TEMPLATE_DEBUG = False

ALLOWED_HOSTS = []

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'project_hub',
        'USER': 'root',
        'PASSWORD': '',
        'HOST': '127.0.0.1',
        'PORT': '3306',
        'OPTIONS': {
              'init_command': 'SET storage_engine=INNODB,character_set_connection=utf8,collation_connection=utf8_unicode_ci'
        }
    }
}

TEMPLATE_LOADERS = (
    ('django.template.loaders.cached.Loader', (
        'django.template.loaders.filesystem.Loader',
        'django.template.loaders.app_directories.Loader',
    )),
)

CACHES = {
    'default': {
        'BACKEND': 'caching.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:21633',
        'OPTIONS': {
            'DB': 2,
            'PASSWORD': 'tonyhwilson'
        },
    },
}

BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'smtp.webfaction.com'
EMAIL_HOST_USER = 'microbubble'
EMAIL_HOST_PASSWORD = 'dav1db0w1e'
DEFAULT_FROM_EMAIL = 'noreply@builtbyhowie.co.uk'
SERVER_EMAIL = 'noreply@builtbyhowie.co.uk'

STATIC_ROOT = '/home/microbubble/webapps/hub_static/'
MEDIA_ROOT = '/home/microbubble/webapps/hub_media/'
