from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from django.template.response import TemplateResponse
from django.views.generic import RedirectView
from users.views import CustomLoginView, password_change_done

urlpatterns = patterns('',

    url(r'^robots.txt$',
        lambda request: TemplateResponse(
            request,
            template='robots.txt',
            content_type='text/plain',
        )
    ),

    url(r'^favicon\.ico$', RedirectView.as_view(url='/static/images/bbh_logo.png')),

    url(r'^login/$',     CustomLoginView.as_view(), name='login'),
    url(r'^logout/$', 'django.contrib.auth.views.logout_then_login', {'login_url': '/login/'}, name='logout'),
    url(r'^password_change/$', 'django.contrib.auth.views.password_change', name='password_change'),
    url(r'^password_change/done/$', password_change_done, name='password_change_done'),
    url(r'^password_reset/$', 'django.contrib.auth.views.password_reset', name='password_reset'),
    url(r'^password_reset/done/$', 'django.contrib.auth.views.password_reset_done', name='password_reset_done'),

    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>.+)/$',
        'django.contrib.auth.views.password_reset_confirm', name='password_reset_confirm'),

    url(r'^password_reset/complete/$', 'django.contrib.auth.views.password_reset_complete', name='password_reset_complete'),

    url(r'^manage/', include(admin.site.urls)),

    url(r'^api/v1/', include('api.urls')),
    url(r'^', include('frontend.urls')),
)

if settings.DEBUG:
    from django.conf.urls.static import static
    urlpatterns += patterns(
        static(settings.STATIC_URL, document_root=settings.STATIC_ROOT),
    )

    urlpatterns += patterns('django.views.static',
        (r'media/(?P<path>.*)', 'serve', {'document_root': settings.MEDIA_ROOT}),
    )