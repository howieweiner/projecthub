angular.module('projecthub.services', ['restangular'])

  .factory('RESTService', function (Restangular) {
    return Restangular.withConfig(function (RestangularConfigurer) {
      RestangularConfigurer.setBaseUrl('/api/v1/');
      // Django expects trailing slashes
      RestangularConfigurer.setRequestSuffix('/');
      // DRF returns data nested with meta data for lists..
      RestangularConfigurer.addResponseInterceptor(function (data) {
        if (data.results != undefined) {
          var extractedData = data.results;
          extractedData.meta = {
            count: data.count,
            next: data.next,
            previous: data.previous
          };
          return extractedData;
        }
        else {
          return data;
        }
      });
    });
  })

  .factory('EnvService', function (RESTService) {
    return  {
      get: function () {
        return RESTService.oneUrl('user', '/api/v1/env/').get().then(function (env) {
          return env;
        })
      }
    }
  })

  .factory('CurrentUserService', function (RESTService) {
    return  {
      get: function () {
        return RESTService.oneUrl('user', '/api/v1/users/current/').get().then(function (user) {
          return user;
        })
      }
    }
  })

  .factory('UserService', function (RESTService) {
    return RESTService.service('users');
  })

  .factory('ProjectService', function (RESTService) {
    return RESTService.service('projects');
  })

  .factory('DiscussionService', function (RESTService) {
    return RESTService.service('discussions');
  })
;