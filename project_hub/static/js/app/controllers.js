angular.module('projecthub.controllers', ['ui.bootstrap', 'ui.select', 'textAngular', 'projecthub.services'])

  .controller('NavCtrl', function ($scope, $timeout, EnvService, ProjectService, CurrentUserService) {

    $scope.nav = {
      'isHidden': false
    };

    $scope.userNav = {
      'isOpen': false
    };

    EnvService.get().then(function (env) {
      $scope.env = env;
    });

    CurrentUserService.get().then(function (user) {
      $scope.user = user;
    });

    ProjectService.getList().then(function (projects) {
      $scope.projects = projects;
    });

    $scope.toggleNav = function ($event) {
      $event.preventDefault();
      $event.stopPropagation();
      $scope.nav.isHidden = !$scope.nav.isHidden;
    };

    $scope.toggleUserNav = function ($event) {
      $event.preventDefault();
      $event.stopPropagation();
      $scope.userNav.isOpen = !$scope.userNav.isOpen;
    };

    $scope.activateUserNavToggle = function ($event) {
      $event.preventDefault();
      $event.stopPropagation();

      // for ui-bootstrap dropdowns to work, we need to activate via the anchor click event
      var anchor = document.getElementById('user-nav-toggle');

      // see http://stackoverflow.com/questions/22447374/how-to-trigger-ng-click-angularjs-programmatically
      $timeout(function () {
        angular.element(anchor).triggerHandler('click');
      }, 0);
    };
  })

  .controller('HubCtrl', function ($scope, $rootScope, $state) {

  })

  .controller('ActivityCtrl', function ($scope, $rootScope, $state, viewDetails) {
    $rootScope.viewDetails = viewDetails;

    // TODO: List latest discussions
  })

  .controller('ViewProjectCtrl', function ($scope, $rootScope, $stateParams, viewDetails, project, discussions) {
    $rootScope.viewDetails = viewDetails;
    $scope.project = project;
    $scope.project.discussions = discussions;
  })

  .controller('ViewDiscussionCtrl', function ($scope, $rootScope, $state, $stateParams, viewDetails, discussion, RESTService) {

    $rootScope.viewDetails = viewDetails;

    // TODO: Refactor - need isNewDiscussion and isNewComment
    $scope.isNew = $stateParams.new;

    $scope.newComment = {};

    $scope.discussion = discussion;

    $scope.addComment = function () {
      $scope.$broadcast('show-errors-check-validity');
      if ($scope.discussionCommentForm.$valid) {

        // TODO: can we externalise this?
        var discussionCommentsService =
          RESTService.service('comments', RESTService.one('discussions', $stateParams.discussionId));

        discussionCommentsService.post($scope.newComment).then(function () {
          $state.reinit();
        });
      }
    }
  })

  .controller('AddDiscussionCtrl', function (
    $scope, $rootScope, $state, $stateParams, viewDetails, DiscussionService, ProjectService) {

    $rootScope.viewDetails = viewDetails;

    $scope.projectMembers = [];

    // get list of project members, removing thew logged in user. This gives us the list of potential discussion members
    ProjectService.one($stateParams.projectId).get().then(function (project) {
      $scope.projectMembers = project.participants;
      _.remove($scope.projectMembers, function (member) {
        return member.id === $scope.user.id;
      });
    });

    initialise();

    function initialise() {
      $scope.discussion = {
        project: $stateParams.projectId,
        members: [],
        comment: null
      };
    }

    $scope.save = function () {
      $scope.$broadcast('show-errors-check-validity');
      if ($scope.discussionForm.$valid) {
        // NOTE: Discussion objects don't actually have a comment attribute, but the first message is submitted with the form
        // at creation time. The back-end takes care of saving the nested model Discussion->Message
        DiscussionService.post($scope.discussion).then(function (discussion) {
          // reset model/form for next use
          initialise();
          // indicate that this is a new discussion so that view can animate
          $state.go('hub.discussion',
            {projectId: $stateParams.projectId, discussionId: discussion.id, new: true});
        });
      }
    }
  })
;