angular.module('projecthub', [
  'ngCookies', 'ngAnimate', 'ngSanitize', 'ui.router', 'ui.bootstrap.showErrors', 'textAngular',
  'projecthub.controllers', 'projecthub.directives', 'projecthub.services'])

  .run(function ($http, $rootScope, $location, $cookies, $state, $stateParams) {

    $http.defaults.headers.common['Cache-Control'] = 'no-cache';
    $http.defaults.headers.common['X-CSRFToken'] = $cookies.csrftoken;

    // see http://angular-ui.github.io/ui-router/sample/app/app.js
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
  })

  .config(function ($httpProvider, $stateProvider, $urlRouterProvider, $locationProvider, $provide, uiSelectConfig) {

    $httpProvider.defaults.headers.patch = {
      'Content-Type': 'application/json;charset=utf-8'
    };

    $httpProvider.defaults.cache = false;
    if (!$httpProvider.defaults.headers.get) {
      $httpProvider.defaults.headers.get = {};
    }
    // disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = '0';


    // see https://github.com/angular-ui/ui-router/issues/582
    // Bug in $state.reload() - resolves are refreshed, but Controller is not re-initialised
    $provide.decorator('$state', function ($delegate) {
      $delegate.reinit = function () {
        this.go('.', null, { reload: true });
      };
      return $delegate;
    });

    // set theme for dropdowns
    uiSelectConfig.theme = 'bootstrap';

    // set default toolbar for rich text editor
    $provide.decorator('taOptions', ['$delegate', function (taOptions) {
      taOptions.toolbar = [
        ['bold', 'italics', 'underline'],['ul', 'ol']
      ];
      taOptions.classes = {
                focussed: 'focussed',
                toolbar: 'btn-toolbar',
                toolbarGroup: 'btn-group',
                toolbarButton: 'btn btn-default btn-sm',
                toolbarButtonActive: 'active',
                disabled: 'disabled',
                textEditor: 'form-control',
                htmlEditor: 'form-control'
            };
      return taOptions;
    }]);

    // use the HTML5 History API
    $locationProvider.html5Mode(true);

    $urlRouterProvider.when("", "/hub/activity");
    $urlRouterProvider.when("/", "/hub/activity");
    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/hub/activity');

    $stateProvider
      .state('hub', {
        abstract: true,
        url: "/hub",
        templateUrl: 'templates/hub.html',
        controller: 'HubCtrl'
      })

      .state('hub.activity', {
        url: "/activity",
        templateUrl: 'templates/activity.html',
        controller: 'ActivityCtrl',
        resolve: {
          viewDetails: function () {
            return {
              title: 'Recent activity'
            }
          }
        }
      })

      .state('hub.project', {
        url: "/:projectId",
        templateUrl: 'templates/project/detail.html',
        controller: 'ViewProjectCtrl',
        resolve: {
          project: function ($stateParams, ProjectService) {
            return ProjectService.one($stateParams.projectId).get();
          },
          discussions: function($stateParams, DiscussionService) {
            return DiscussionService.getList();
          },
          viewDetails: function (project) {
            return {
              title: project.title
            }
          }
        }
      })

      .state('hub.discussion', {
        url: "/:projectId/discussion/:discussionId?new",
        templateUrl: 'templates/discussion/detail.html',
        controller: 'ViewDiscussionCtrl',
        resolve: {
          discussion: function ($stateParams, DiscussionService) {
            return DiscussionService.one($stateParams.discussionId).get();
          },
          viewDetails: function (discussion) {
            return {
              title: discussion.topic
            }
          }
        }
      })

      .state('hub.addDiscussion', {
        url: "/:projectId/discussion",
        templateUrl: 'templates/discussion/form.html',
        controller: 'AddDiscussionCtrl',
        resolve: {
          viewDetails: function () {
            return {
              title: 'New discussion'
            }
          }
        }
      })
    ;
  })

/**
 * AngularJS default filter with the following expression:
 * "person in people | filter: {name: $select.search, age: $select.search}"
 * performs a AND between 'name: $select.search' and 'age: $select.search'.
 * We want to perform a OR.
 */
  .filter('propsFilter', function () {
    return function (items, props) {
      var out = [];

      if (angular.isArray(items)) {
        items.forEach(function (item) {
          var itemMatches = false;

          var keys = Object.keys(props);
          for (var i = 0; i < keys.length; i++) {
            var prop = keys[i];
            var text = props[prop].toLowerCase();
            if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
              itemMatches = true;
              break;
            }
          }

          if (itemMatches) {
            out.push(item);
          }
        });
      } else {
        // Let the output be the input untouched
        out = items;
      }

      return out;
    };
  });