angular.module('projecthub.directives', [])

  .directive('autoFocus', function () {
    return {
      restrict: 'A', // only matches attribute name

      link: function (scope, element) {
        element[0].focus();
      }
    }
  })
;