from django.contrib.auth.decorators import user_passes_test, login_required
from django.utils.decorators import method_decorator
from users.permissions import has_access


class UserAccessMixin(object):
    '''
    Mixin to test if user has access rights to the app
    '''
    @method_decorator(login_required(login_url='/login'))
    @method_decorator(user_passes_test(has_access, login_url='/logout'))
    def dispatch(self, *args, **kwargs):
        return super(UserAccessMixin, self).dispatch(*args, **kwargs)
