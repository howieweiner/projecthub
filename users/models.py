from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.db import models
from users.managers import AppUserManager


class AppUser(PermissionsMixin, AbstractBaseUser):
    '''
    Model that represents a user of the App. Sets email as username, and expires password by default
    '''
    class Meta:
        # ordering = ['last_name', 'first_name']
        verbose_name = 'User'

    first_name = models.CharField('First Name', max_length=50)
    last_name = models.CharField('Last Name', max_length=50)
    email = models.EmailField(
        verbose_name='email address',
        max_length=254,
        unique=True,
        db_index=True,
    )

    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)

    password_expired = models.BooleanField(default=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'last_name']

    objects = AppUserManager()

    @property
    def get_full_name(self):
        return self.first_name + u' ' + self.last_name

    def get_short_name(self):
        return self.get_full_name

    @property
    def initials(self):
        return (self.first_name[:1] + self.last_name[:1]).upper()

    def __unicode__(self):
        return self.get_full_name

    @property
    def password_not_expired(self):
        return not self.password_expired