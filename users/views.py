from class_based_auth_views.views import LoginView
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.template.response import TemplateResponse
from django.contrib.auth import login
from users.forms import CrispyAuthenticationForm


class CustomLoginView(LoginView):
    form_class = CrispyAuthenticationForm

    def form_valid(self, form):
        """
        Check that password has not expired. If, so log user out and redirect password change form
        """
        user = form.get_user()

        if user.password_expired:
            login(self.request, user)
            return HttpResponseRedirect(reverse('password_change'))
        return super(CustomLoginView, self).form_valid(form)


@login_required
def password_change_done(request,
                         template_name='registration/password_change_done.html',
                         current_app=None, extra_context=None):
    '''
    Override default password change done view, to reset flag on user
    '''
    user = request.user
    user.password_expired = False
    user.save()

    context = {}
    if extra_context is not None:
        context.update(extra_context)
    return TemplateResponse(request, template_name, context,
                            current_app=current_app)
