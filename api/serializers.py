from django.contrib.auth import get_user_model
from rest_framework import serializers
from hub.models import Project, Discussion, DiscussionComment

User = get_user_model()

class UserSerializer(serializers.ModelSerializer):

    full_name = serializers.CharField(source='get_full_name', read_only=True)
    initials = serializers.CharField(source='initials', read_only=True)

    class Meta:
        model = User
        fields = ('id', 'full_name', 'first_name', 'last_name', 'email', 'initials',)


class DiscussionCommentSerializer(serializers.ModelSerializer):

    class Meta:
        model = DiscussionComment


class FlatDiscussionSerializer(serializers.ModelSerializer):

    class Meta:
        model = Discussion
        fields = ('id', 'topic', 'created', 'project',)


class NestedDiscussionSerializer(serializers.ModelSerializer):

    class Meta:
        model = Discussion
        fields = ('id', 'topic', 'created', 'project', 'comments',)
        read_only_fields = ('comments',)
        depth = 1


class ProjectSerializer(serializers.ModelSerializer):

    # discussions = FlatDiscussionSerializer(many=True)
    participants = UserSerializer(many=True)

    class Meta:
        model = Project
        fields = ('id', 'title', 'participants',)