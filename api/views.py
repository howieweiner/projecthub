from django.contrib.auth import get_user_model
from django.core.exceptions import ObjectDoesNotExist
from rest_framework import viewsets
from rest_framework.decorators import list_route
from rest_framework.parsers import JSONParser
from rest_framework.response import Response
from rest_framework.views import APIView
from api.serializers import ProjectSerializer, UserSerializer, NestedDiscussionSerializer, DiscussionCommentSerializer, \
    FlatDiscussionSerializer
from hub.models import Project, Discussion, DiscussionComment, DiscussionMember
from project_hub import settings

User = get_user_model()


class UserViewSet(viewsets.ModelViewSet):
    model = User
    serializer_class = UserSerializer

    @list_route()
    def current(self, request, pk=None):
        serializer = self.serializer_class(request.user)
        return Response(serializer.data)


class EnvView(APIView):
    '''
    Provide environment details, such as server
    '''
    def get(self, request, format=None):
        data = {
            'server': settings.ENV.title()
        }
        return Response(data)


class ProjectViewSet(viewsets.ReadOnlyModelViewSet):
    model = Project
    serializer_class = ProjectSerializer

    def get_queryset(self):
        '''
        Restrict queryset to those projects that the logged in user is a member of
        '''
        return self.request.user.projects.all()


class DiscussionViewSet(viewsets.ModelViewSet):
    model = Discussion

    def get_queryset(self):
        '''
        Restrict queryset to those discussions that the logged in user is a member of
        '''
        return self.request.user.belongs_to_discussions.all()

    def pre_save(self, obj):
        """
        Set the object's owner, based on the incoming request.
        """
        obj.owner = self.request.user

    def post_save(self, discussion, created=False):
        """
        Saving nested models is bleeding edge in DRF. We do it manually here. Once a Discussion has been created, we
        also save the first comment that will have been submitted with the form, and add members to the discussion too
        """
        comment = self.request.DATA.get('comment', None)
        if comment:
            data = {
                'text': comment,
                'created_by': self.request.user.id,
                'discussion': discussion.id
            }

            serializer = DiscussionCommentSerializer(data=data)
            if serializer.is_valid():
                serializer.save()

        # See if any discussion members have been added
        member_data = self.request.DATA.get('members', None)
        if member_data:
            for data in member_data:
                # ensure that the member does belong to the project
                try:
                    member = discussion.project.participants.get(pk=data['id'])
                    dm = DiscussionMember(member=member, discussion=discussion)
                    dm.save()
                except ObjectDoesNotExist:
                    pass

        dm = DiscussionMember(member=self.request.user, discussion=discussion)
        dm.save()

    def get_serializer_class(self):
        if self.request.method in ['PATCH', 'POST', 'PUT']:
            return FlatDiscussionSerializer
        return NestedDiscussionSerializer


class DiscussionCommentViewSet(viewsets.ViewSet):
    queryset = DiscussionComment.objects.all()
    serializer_class = DiscussionCommentSerializer

    def list(self, request, discussion_pk=None):
        queryset = self.queryset.filter(discussion=discussion_pk)
        serializer = self.serializer_class(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None, discussion_pk=None):
        queryset = self.queryset.get(pk=pk, discussion=discussion_pk)
        serializer = self.serializer_class(queryset)
        return Response(serializer.data)

    def create(self, request, discussion_pk=None):
        # the POST will contain the comment text, The discussion id is part of the nested URL structure
        data = JSONParser().parse(request)

        data.update({
            'created_by': self.request.user.id,
            'discussion': discussion_pk
        })

        serializer = self.serializer_class(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=400)
