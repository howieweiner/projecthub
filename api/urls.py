from django.conf.urls import include, url
from rest_framework_nested import routers
from api.views import ProjectViewSet, UserViewSet, DiscussionViewSet, DiscussionCommentViewSet, EnvView

router = routers.SimpleRouter()
router.register(r'users', UserViewSet)
router.register(r'projects', ProjectViewSet)
router.register(r'discussions', DiscussionViewSet)

discussion_router = routers.NestedSimpleRouter(router, r'discussions', lookup='discussion')
discussion_router.register(r'comments', DiscussionCommentViewSet)

urlpatterns = [
    url(r'^env/', EnvView.as_view()),
    url(r'^', include(router.urls)),
    url(r'^', include(discussion_router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]