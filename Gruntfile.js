'use strict';

module.exports = function (grunt) {
  // load all grunt tasks matching the `grunt-*` pattern
  require('load-grunt-tasks')(grunt);

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
//    srcFiles: [
//      'src/*.js'
//    ],
//    karma: {
//      unit: {
//        configFile: 'karma.conf.js',
//        singleRun: true
//      }
//    },
//    jshint: {
//      options: {
//        jshintrc: '.jshintrc'
//      },
//      all: [
//        'Gruntfile.js',
//        'src/*.js',
//        'test/*.js'
//      ]
//    },
    concat: {
      app: {
        src: ['project_hub/static/js/app/**/*.js'],
        dest: 'build/static/js/projecthub.js'
      },
      vendor: {
        src: [
          'bower_components/angular/angular.js',
          'bower_components/angular-sanitize/angular-sanitize.js',
          'bower_components/angular-cookies/angular-cookies.js',
          'bower_components/angular-bootstrap/ui-bootstrap-tpls.js',
          'bower_components/angular-mocks/angular-mocks.js',
          'bower_components/angular-ui-router/release/angular-ui-router.js',
          'bower_components/lodash/dist/lodash.compat.js',
          'bower_components/restangular/dist/restangular.js',
          'bower_components/angular-bootstrap-show-errors/src/showErrors.js',
          'bower_components/angular-animate/angular-animate.js',
          'bower_components/angular-ui-select/dist/select.js',
          'bower_components/textAngular/dist/textAngular.min.js'
        ],
        dest: 'build/static/js/lib.js'
      }
    },
    uglify: {
      options: {
        banner: '/*\n <%= pkg.name %> v<%= pkg.version %> \n (c) 2014 Built By Howie Ltd. http://builtbyhowie.co.uk \n License: MIT \n*/\n'
      },
      app_plus_templates: {
        files: {'dist/static/js/projecthub-tpls.min.js': ['build/static/js/projecthub.js', '<%= ngtemplates.projecthub.dest %>']}
      },
      vendor: {
        files: {'dist/static/js/lib.min.js': ['build/static/js/lib.js']}
      }
    },
    ngtemplates: {
      projecthub: {
        cwd: 'project_hub/static/js/app',
        src: 'templates/**/*.html',
        dest: 'build/static/js/tpl.js',
        htmlmin: {
          collapseBooleanAttributes: true,
          collapseWhitespace: true,
          removeAttributeQuotes: true,
          removeComments: true, // Only if you don't use comment directives!
          removeEmptyAttributes: true,
          removeRedundantAttributes: true,
          removeScriptTypeAttributes: true,
          removeStyleLinkTypeAttributes: true
        }
      }
    },
    autoprefixer: {
      dist: {
        files: {
          'build/static/css/projecthub.css' : 'project_hub/static/css/app.css'
        }
      }
    },
    watch: {
      options: {
        livereload: true
      },
      javascript: {
        files: ['project_hub/static/js/app/**/*.js', 'project_hub/static/js/app/**/*.html'],
        tasks: ['ngtemplates', 'concat', 'autoprefixer']
      }
    }
  });

//  grunt.registerTask('test', ['jshint', 'karma:unit']);
//  grunt.registerTask('dist', ['test', 'uglify']);
  grunt.registerTask('build', ['ngtemplates', 'concat', 'autoprefixer']);
  grunt.registerTask('default', ['watch']);
};