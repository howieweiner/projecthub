from django.views.generic import TemplateView
from users.mixins import UserAccessMixin


class FrontendView(UserAccessMixin, TemplateView):
    template_name = 'frontend/app.html'
