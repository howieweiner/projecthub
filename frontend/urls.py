from django.conf.urls import patterns, url
from frontend.views import FrontendView

urlpatterns = patterns('',
    url(r'^$', FrontendView.as_view(), name='dashboard'),

    # rewirte URLs for HTML History API
    url(r'^hub/', FrontendView.as_view()),
)
